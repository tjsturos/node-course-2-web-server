const express = require('express');
const hbs = require('hbs');
const fs = require('fs');

var app = express();
const port = process.env.PORT || 3000;

app.set('view engine', hbs);

app.use((req, res, next) => {
  var now = new Date().toString();
  var log = `${now}: ${req.method} ${req.url}\n`;
  fs.appendFile("log.txt", log, (err) => {
    if(err) console.log("Could not append to file!");
  })
  console.log(log);
  next();
});


hbs.registerPartials(__dirname + "/views/partials");
//app.use((req, res, next) => res.render("maintainence.hbs"));
app.use(express.static(__dirname + "/public"));

hbs.registerHelper('getCurrentYear', () => {
  return new Date().getFullYear()
});

hbs.registerHelper('screamIt', (string) => {
  return string.toUpperCase();
});

app.get("/", (req, res) => {
  res.render("home.hbs", {
    siteTitle: "Tyler's WS",
    pageTitle: "Home Page",
    welcomeMessage: "Hey! I am quite glad to see you!"
  });
});

app.get("/about", (req, res) => {
  res.render("about.hbs", {
    siteTitle: "Tyler's WS",
    pageTitle: "About Page"
  });
});

app.get("/projects", (req, res) => {
  res.render("projects.hbs", {
    siteTitle: "Tyler's Project Portfolio",
    pageTitle: "Project Portfolio"
  });
})


app.get("/help", (req, res) => {
  res.render("help.hbs", {
   siteTitle: "Tyler's WS",
   pageTitle: "Help Page"
 });
});



app.get("/error", (req, res) => {
  res.send({
    errorMessage: "Something went wrong!"
  })
})
app.listen(port, () => {
  console.log(`Server is up on port ${port}.`);
});
